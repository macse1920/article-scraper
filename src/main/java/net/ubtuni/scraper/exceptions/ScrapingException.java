package net.ubtuni.scraper.exceptions;

public class ScrapingException extends Exception
{
	public ScrapingException(final String message)
	{
		super(message);
	}
}