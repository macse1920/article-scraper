package net.ubtuni.scraper.services;

import java.io.IOException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import net.ubtuni.scraper.exceptions.ScrapingException;
import net.ubtuni.scraper.models.Article;

public abstract class AbstractScraper
{

	private StringBuilder stringBuilder;

	public AbstractScraper()
	{
		stringBuilder = new StringBuilder();
	}

	public abstract Article parse(final String url) throws ScrapingException, IOException;

	public Document getDocument(final URL url) throws IOException
	{
		return Jsoup.parse(url, 8000000);
	}

	protected void checkUrlPrecondition(String url) throws ScrapingException
	{
		if (url == null || url.isEmpty())
		{
			throw new ScrapingException("URL is required");
		}
	}

	protected Article buildArticle(String title, String content)
	{
		return new Article(title, content);
	}

	protected StringBuilder getContentBuilder()
	{
		return stringBuilder;
	}
}