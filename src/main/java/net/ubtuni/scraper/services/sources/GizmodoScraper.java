package net.ubtuni.scraper.services.sources;

import java.io.IOException;
import java.net.URL;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Service;

import net.ubtuni.scraper.annotations.UrlFormat;
import net.ubtuni.scraper.exceptions.ScrapingException;
import net.ubtuni.scraper.models.Article;
import net.ubtuni.scraper.services.AbstractScraper;

@Service
@UrlFormat(value = "https://gizmodo.com")
public class GizmodoScraper extends AbstractScraper
{

	@Override
	public Article parse(final String url) throws ScrapingException, IOException
	{
		checkUrlPrecondition(url);

		StringBuilder contentBuilder = new StringBuilder();

		final Document document = getDocument(new URL(url));

		final String parsedTitle = document.getElementsByTag("header")
				.first()
				.getElementsByTag("h1")
				.first()
				.text();

		final Element postContent = document.getElementsByClass("js_post-content").first();

		postContent.getElementsByTag("p")
				.forEach(element -> {
					contentBuilder.append(element.text());
				});

		return buildArticle(parsedTitle, contentBuilder.toString());
	}
}