package net.ubtuni.scraper.services.sources;

import java.io.IOException;
import java.net.URL;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Service;

import net.ubtuni.scraper.annotations.UrlFormat;
import net.ubtuni.scraper.exceptions.ScrapingException;
import net.ubtuni.scraper.models.Article;
import net.ubtuni.scraper.services.AbstractScraper;

@Service
@UrlFormat(value = "https://www.theverge.com/")
public class TheVergeScraper extends AbstractScraper
{
	@Override
	public Article parse(final String url) throws ScrapingException, IOException
	{
		checkUrlPrecondition(url);

		StringBuilder contentBuilder = new StringBuilder();

		final Document document = getDocument(new URL(url));

		final String parsedTitle = document.getElementsByClass("c-page-title").text();

		final Element element = document.getElementsByClass("c-entry-content").first();

		element.getElementsByTag("p").forEach(el -> {

			if (el.hasText() && !el.text().trim().equals("Related"))
			{
				contentBuilder.append(el.text());

			}

		});

		return buildArticle(parsedTitle, contentBuilder.toString());
	}
}