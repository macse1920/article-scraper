package net.ubtuni.scraper.controllers;

import java.io.IOException;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.ubtuni.scraper.exceptions.ScrapingException;
import net.ubtuni.scraper.services.AbstractScraper;

@RestController
@RequestMapping(value = "/scraper/article")
public class MainController extends AbstractController
{

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity parseContent(@RequestParam(value = "link") String articleLink)
	{
		try
		{
			final AbstractScraper scraper = getScraper(articleLink);
			return ResponseEntity.ok(scraper.parse(articleLink));
		}
		catch (ScrapingException | IOException e)
		{
			return ResponseEntity.badRequest().body(e.getLocalizedMessage());
		}
	}
}