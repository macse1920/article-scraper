package net.ubtuni.scraper.controllers;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import net.ubtuni.scraper.annotations.UrlFormat;
import net.ubtuni.scraper.exceptions.ScrapingException;
import net.ubtuni.scraper.services.AbstractScraper;

@Component
public class AbstractController
{
	@Autowired
	private ApplicationContext context;


	protected AbstractScraper getScraper(String link) throws ScrapingException
	{

		AtomicReference<AbstractScraper> abstractScraper = new AtomicReference<>();

		context.getBeansWithAnnotation(UrlFormat.class)
				.forEach((s, o) -> {
					AbstractScraper scraper = ((AbstractScraper) o);

					if (link.startsWith(scraper.getClass().getAnnotation(UrlFormat.class).value()))
					{
						abstractScraper.set(scraper);
					}

				});

		return Optional.ofNullable(abstractScraper.get())
				.orElseThrow(() -> new ScrapingException("No scraper found for link :" + link));
	}

}